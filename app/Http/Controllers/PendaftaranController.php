<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Pendaftaran;
use App\Pasien;
use App\Setting;
use DateTime;
use App\Events\MessageSent;
use Auth;

class PendaftaranController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function openAntrian(Request $request) 
    {
        $request->session()->put('statusAntrian', 'open');
        $data = [
             'status_antrian' => 'open'
        ];
        $update = Setting::where('id', 1)->update($data);

        $statusAntrian = $request->session()->get('statusAntrian');

        return response()->json(['statusAntrian' => $statusAntrian], 200);   
    }

    public function getStatusAntrian(Request $request) 
    {
        $setting = Setting::where('id', 1)->first();

        // $statusAntrian = $request->session()->get('statusAntrian');
        $statusAntrian = $setting->status_antrian;
        $pointer = $setting->status_pointer;
        // $pointer = $request->session()->get('pointer');
        return response()->json(['statusAntrian' => $statusAntrian, 'pointer' => $pointer], 200);
    }

    public function getAntrian()
    {
        $antrian = DB::select("
              SELECT pendaftarans.id, pendaftarans.identitas_diri, pasiens.nama, pasiens.alamat, YEAR(CURRENT_DATE) - YEAR(pasiens.tanggal_lahir) - (DATE_FORMAT(CURRENT_DATE, '%m%d') < DATE_FORMAT(pasiens.tanggal_lahir, '%m%d')) as umur,
                pasiens.jenis_kelamin,pasiens.no_hp, pendaftarans.diagnosa, pendaftarans.catatan, pendaftarans.status
              FROM pendaftarans
              LEFT JOIN pasiens ON pasiens.identitas_diri = pendaftarans.identitas_diri
              ORDER BY pendaftarans.id");
        return response()->json(['antrian' => $antrian], 200);
    }
    public function closeAntrian(Request $request) 
    {
        Pendaftaran::truncate();
        $request->session()->put('statusAntrian', 'open');
        $data = [
             'status_antrian' => 'close', 
             'status_pointer' => 0
        ];
        $update = Setting::where('id', 1)->update($data);
        $request->session()->forget('pointer');
        $request->session()->put('pointer', null);
        $request->session()->forget('statusAntrian');

        return response()->json(['message' => 'success'], 200);   
    }

    public function setCounterAntrian(Request $request) 
    {
        $pointer = $request->input('pointer');
        $now = date('Y-m-d H:i:s');
        $data = [
             'status_pointer' => $pointer
        ];
        // $request->session()->put('pointer', $pointer);
        $update = Setting::where('id', 1)->update($data);
        
        $pasien = Pendaftaran::where('id', $pointer)->first();
        if ($pasien->status == "MENUNGGU") {
            $data = ['status' => 'SEDANG DIPERIKSA', 
                    'waktu_mulai_periksa' => $now];
            $update = Pendaftaran::where('id', $pointer)->update($data);
        }
        broadcast(new MessageSent("nextantrian"))->toOthers();   
        return response()->json(['pointer' => $pointer], 200); 
    }

    public function getNama(Request $request) 
    {
        $nama = DB::select("SELECT nama FROM pasiens ORDER BY nama"); 
        return response()->json(['nama' => $nama], 200); 
    }

    public function getId(Request $request) 
    {
        $id = DB::select("SELECT identitas_diri FROM pasiens ORDER BY identitas_diri"); 
        return response()->json(['id' => $id], 200);
    }
    public function getPasienId(Request $request) 
    {
        $identitas_diri = $request->input('identitas_diri');
        $pasien = Pasien::where('identitas_diri', $identitas_diri)->first();
        $last = DB::table('pendaftarans')->orderBy('id','desc')->first();
        return response()->json(['pasien' => $pasien,'last'=>$last], 200);
    }
    public function getPasienNama(Request $request) 
    {
        $nama = $request->input('nama');
        $pasien = Pasien::where('nama', $nama)->first();
        $last = DB::table('pendaftarans')->orderBy('id','desc')->first();
        return response()->json(['pasien' => $pasien,'last'=>$last], 200);
    }
    public function getDataSetting(Request $request) {
        $setting = Setting::where('id', 1)->first();
        return response()->json(['setting' => $setting], 200);
    }
    public function settingAntrian(Request $request) {
        $data = [  
            'jam_buka' => $request->input('jam_buka'),
            'waktu_tunggu' => $request->input('waktu_tunggu'),
            'batas_pasien' => $request->input('batas_pasien')

        ];   
         
        Setting::where('id', 1)->update($data);
        return response()->json(['message' => 'success'], 200);
    }
    public function getEditAntrian(Request $request) {
        $id = $request->input('id');
        $pendaftaran = Pendaftaran::where('id', $id)->first();
        return response()->json(['pendaftaran' => $pendaftaran], 200);
    }
     public function addAntrian(Request $request) 
    {
        $user = Auth::user();
        $now = date('Y-m-d H:i:s');
        $data = [
             'identitas_diri' => $request->identitas_diri, 
             'waktu_pendaftaran' => $now
        ];
        $masuk = Pendaftaran::insert($data);

        //$antrian = DB::table('pendaftarans')->orderBy('id','desc')->first();
        
        broadcast(new MessageSent("antrianbaru"))->toOthers();

        return response()->json(['tanggal_pendaftaran' => $now], 200);
    }
     public function editAntrian(Request $request) 
    {
        $status = $request->input('status');
        $now = date('Y-m-d H:i:s');
        if ($status=="SEDANG DIPERIKSA") 
        {
            $data = [ 
             'waktu_mulai_periksa' => $now, 
             'status' => $status
            ];
        }
        elseif ($status=="SELESAI") {
            $data = [ 
             'waktu_selesai_periksa' => $now, 
             'status' => $status, 
             'catatan' => $request->input('catatan'), 
             'diagnosa' => $request->input('diagnosa')
            ];   
        }
        else {
             $data = [  
             'status' => $status
            ];   
         } 
        
        Pendaftaran::where('id', $request->input('id'))->update($data);

        broadcast(new MessageSent("antrianbaru"))->toOthers();

        return response()->json(['tanggal_pendaftaran' => $now], 200);
    }
    public function selesaipemeriksaan(Request $request) 
    {
        
    }

}