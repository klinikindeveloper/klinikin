<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendaftaran extends Model
{
    protected $table = 'pendaftarans'; 
    protected $primaryKey ='id';
    protected $fillable = ['identitas_diri'];
    public $timestamps = false;
}
