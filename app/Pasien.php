<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    protected $table = 'pasiens'; 
    protected $primaryKey ='id';
    protected $fillable = ['identitas_diri','nama','alamat','no_hp'];
    public $timestamps = false;
}
