

import VueRouter from 'vue-router'

let routes=[
{
        path:'/dashboard',
        component:require('./components/Dashboard')
},
{
        path:'/pendaftaran',
        component:require('./components/Pendaftaran')
},
{
        path:'/items',
        component:require('./components/Items')
}
]

export default new VueRouter({
        routes,
        mode: 'history'
})

