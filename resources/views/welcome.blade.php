<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Ample Admin Template - The Ultimate Multipurpose admin template</title>
        <!-- Bootstrap Core CSS -->
        <link href="{{URL::asset('ampleadmin/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="{{URL::asset('ampleadmin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css')}}" rel="stylesheet">
        <!-- toast CSS -->
        <link href="{{URL::asset('ampleadmin/plugins/bower_components/toast-master/css/jquery.toast.css')}}" rel="stylesheet">
        <!-- morris CSS -->
        <link href="{{URL::asset('ampleadmin/plugins/bower_components/morrisjs/morris.css')}}" rel="stylesheet">
        <!-- chartist CSS -->
        <link href="{{URL::asset('ampleadmin/plugins/bower_components/chartist-js/dist/chartist.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('ampleadmin/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')}}" rel="stylesheet">
        <!-- Calendar CSS -->
        <link href="{{URL::asset('ampleadmin/plugins/bower_components/calendar/dist/fullcalendar.css')}}" rel="stylesheet" />
        <!-- Time Picker -->
        <link href="{{URL::asset('ampleadmin/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet">
        <!-- SELECT2 --> 
        <link href="{{URL::asset('ampleadmin/plugins/bower_components/custom-select/custom-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('ampleadmin/plugins/bower_components/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
        <link href="{{URL::asset('ampleadmin/plugins/bower_components/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" />
        <link href="{{URL::asset('ampleadmin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet" />
        <link href="{{URL::asset('ampleadmin/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet" />
        <link href="{{URL::asset('ampleadmin/plugins/bower_components/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
        <!-- animation CSS -->
        <link href="{{URL::asset('ampleadmin/css/animate.css')}}" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="{{URL::asset('ampleadmin/css/style.css')}}" rel="stylesheet">
        <!-- color CSS -->
        <link href="{{URL::asset('ampleadmin/css/colors/megna-dark.css')}}" id="theme" rel="stylesheet">
        </head>
      <style type="text/css">
            .clockpicker-popover {
                z-index: 999999!important;
            }
            .fix-header #page-wrapper {
                margin-top: 20px;
            }
            a {
                color: #fff ;
                text-decoration: none;
            }
            a:hover {
                color: #fff !important;
                text-decoration: none;
            }
            
            .router-link-exact-active {
                background: rgba(0,0,0,.1)!important;
                color: #fff;
                font-weight: 500;
            }
            .dropdown-menu > li > a:hover {
                background: #0a4b3b!important;
            }
            .daft{
               background-color:#fff!important;
               color: #000!important;
            }

        </style>
    <body class="fix-header">
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
            </svg>
        </div>
        <div id="wrapper">
            <div id="app">
                @include('layouts.menu')
                <router-view></router-view>
            </div> <!-- app -->
        </div>    
        
       <!--  <script src="{{URL::asset('ampleadmin/plugins/bower_components/jquery/dist/jquery.min.js')}}"></script>
        --> <!-- Bootstrap Core JavaScript -->
        <script src="https://code.jquery.com/jquery-2.2.4.js"></script>           
        <script src="{{URL::asset('ampleadmin/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{URL::asset('js/highchart/highcharts.js')}}"></script>
        <!-- Menu Plugin JavaScript -->
        <link href="{{URL::asset('ampleadmin/plugins/bower_components/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
        <link href="{{URL::asset('ampleadmin/css/animate.css')}}" rel="stylesheet">
        
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js')}}"></script>

        <script src="{{URL::asset('ampleadmin/plugins/bower_components/sweetalert/sweetalert.min.js')}}"></script>

        <script src="{{URL::asset('ampleadmin/js/custom.min.js')}}"></script>
            <!--Style Switcher -->
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js')}}"></script>

        <script src="{{URL::asset('ampleadmin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js')}}"></script>
        <script src=/js/app.js></script>
        <!--slimscroll JavaScript -->
        <script src="{{URL::asset('ampleadmin/js/jquery.slimscroll.js')}}"></script>
        <!--Wave Effects -->
        <script src="{{URL::asset('ampleadmin/js/waves.js')}}"></script>
        <!--Counter js -->
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/waypoints/lib/jquery.waypoints.js')}}"></script>
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/counterup/jquery.counterup.min.js')}}"></script>
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/switchery/dist/switchery.min.js')}}"></script>
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/custom-select/custom-select.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('ampleadmin/plugins/bower_components/multiselect/js/jquery.multi-select.js')}}"></script>
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js')}}" type="text/javascript"></script>
        <script type="text/javascript" src="{{URL::asset('ampleadmin/plugins/bower_components/multiselect/js/jquery.multi-select.js')}}"></script>


        <!--Morris JavaScript -->
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/raphael/raphael-min.js')}}"></script>
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/morrisjs/morris.js')}}"></script>
        <!-- chartist chart -->
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/chartist-js/dist/chartist.min.js')}}"></script>
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js')}}"></script>
        <!-- Calendar JavaScript -->
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/moment/moment.js')}}"></script>
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/calendar/dist/fullcalendar.min.js')}}"></script>
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/calendar/dist/cal-init.js')}}"></script>
        <!-- Time Picker -->
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js')}}"></script>
        <!-- Custom Theme JavaScript -->
        <script src="{{URL::asset('ampleadmin/js/custom.min.js')}}"></script>
        <!-- Custom tab JavaScript -->
        <script src="{{URL::asset('ampleadmin/js/cbpFWTabs.js')}}"></script>

        <script type="{{URL::asset('ampleadmin/text/javascript')}}">
        (function() {
            [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
                new CBPFWTabs(el);
            });
        })();
        </script>

        <script src="{{URL::asset('ampleadmin/plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
        <!--Style Switcher -->
        <script src="{{URL::asset('ampleadmin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js')}}"></script>
         <script>
            $("#daftarNamaPasien").select2();
            $("#daftarIdPasien").select2();
            $('.clockpicker').clockpicker({
                donetext: 'Done',
            }).find('input').change(function() {
                console.log(this.value);
            });
        </script>
    </body>

</html>