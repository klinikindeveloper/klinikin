<!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <div class="top-left-part">
                    <!-- Logo -->
                    <a href="index.html">
                     </b>
                        <img src="{{URL::asset('images/iconKlinik.png')}}" width="120" height="40" alt="home"/>
                     </span> </a>
                </div>
                <!-- /Logo -->
                <!-- Search input and Toggle icon -->
                <ul class="nav navbar-top-links navbar-left">

                    <li class="mega-dropdown"> 
                        <router-link to="/dashboard" class="dropdown-toggle waves-effect waves-light" href="javascript:void(0)">
                        <a>
                         
                            <i class="fa fa-home" style="margin-right: 10px;font-size: 25px"></i>
                            <span class="hidden-xs">Dashboard</span> 
                        
                        </a>
                        </router-link>
                        
                    </li>
                    
                    <li class="mega-dropdown"> 
                        <router-link to="/pendaftaran" class="dropdown-toggle waves-effect waves-light" href="javascript:void(0)">
                        <a>
                        
                            <i class="fa fa-users" style="margin-right: 10px;font-size: 20px"></i>
                            <span class="hidden-xs">Daftar Tunggu Pasien</span> 
                        </a>
                        </router-link>
                    
                    </li>
                    <!-- .Megamenu -->
                    <li class="mega-dropdown"> 
                        <a class="dropdown-toggle waves-effect waves-light" href="javascript:void(0)">
                            <i class="fa fa-user-md" style="margin-right: 10px;font-size: 25px"></i>
                            <span class="hidden-xs">Data Pasien</span> 
                        </a>
                    </li>
                    <!-- /.Megamenu -->
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li>
                        <form role="search" class="app-search hidden-sm hidden-xs m-r-10">
                            <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a> </form>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="javascript:void(0)"> <img src="{{URL::asset('images/ava_doctor.png')}}" alt="user-img" width="40" class="img-circle"><b class="hidden-xs">Abdullah</b><span class="caret"></span> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"><img src="{{URL::asset('images/ava_doctor.png')}}" alt="user" /></div>
                                    <div class="u-text">
                                        <h4>Abdullah Umar</h4>
                                        <p class="text-muted">abdullahumar22@gmail.com</p></div>
                                </div>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off"></i> 
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                </form>
                            </li>
                                        
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
                
            </div>
        </div>
        
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
