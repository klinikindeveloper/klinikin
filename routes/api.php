<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['auth.jwt']], function () {
	// Route::get('/antrian','PendaftaranController@getAntrian');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/antrian','PendaftaranController@getAntrian');
Route::get('/openAntrian','PendaftaranController@openAntrian');
Route::get('/closeantrian','PendaftaranController@closeAntrian');
Route::post('/setCounterAntrian','PendaftaranController@setCounterAntrian');
Route::get('/getStatusAntrian','PendaftaranController@getStatusAntrian');
Route::get('/getnama','PendaftaranController@getNama');
Route::get('/getid','PendaftaranController@getId');
Route::get('/getdatasetting','PendaftaranController@getDataSetting');
Route::post('/getpasienid','PendaftaranController@getPasienId');
Route::post('/getpasiennama','PendaftaranController@getPasienNama');
Route::post('/geteditantrian','PendaftaranController@getEditAntrian');
Route::post('/addantrian','PendaftaranController@addAntrian');
Route::post('/editantrian','PendaftaranController@editAntrian');
Route::post('/settingantrian','PendaftaranController@settingAntrian');
Route::post('/signin','UserController@signin');
