<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasiensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasiens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identias_diri')->unique()->index();
            $table->string('nama',50); 
            $table->text('alamat');
            $table->string('tempat_lahir',50);
            $table->string('tanggal_lahir',25); 
            $table->string('jenis_kelamin',10);
            $table->string('no_hp',15);
            $table->string('password',50); 
            $table->string('remember_token',255); 
            $table->string('firebase_token',255); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasiens');
    }
}
