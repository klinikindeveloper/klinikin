-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2018 at 03:38 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_klinikin`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(24, '2014_10_12_000000_create_users_table', 1),
(25, '2014_10_12_100000_create_password_resets_table', 1),
(26, '2018_04_20_094706_create_pendaftarans_table', 1),
(27, '2018_04_21_060034_create_pasiens_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pasiens`
--

CREATE TABLE `pasiens` (
  `id` int(10) UNSIGNED NOT NULL,
  `identitas_diri` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firebase_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pasiens`
--

INSERT INTO `pasiens` (`id`, `identitas_diri`, `nama`, `alamat`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `no_hp`, `password`, `remember_token`, `firebase_token`, `created_at`, `updated_at`) VALUES
(1, '3211181409940006', 'Indra Hadi Setiadi', 'Lingkungan Cilengkrang RT 01/17, kelurahan situ kecamatan sumedang utara, kabupaten sumedang 45323.', 'Sumedang', '1994-09-14', 'Laki-Laki', '082116479393', '', '', '', NULL, NULL),
(3, '123', 'kubang', 'kucing', '', '1995-04-30', 'Perempuan', '010101', '', '', '', NULL, NULL),
(4, '10212130', 'abdullah', 'Jalan gagak dalam no 177 RT 09/18, Kelurahan Situ, Kecamatan Sumedang Utara, Kabupaten Sumedang 45323', 'TASIKMALAYA', '1994-07-22', 'Laki-Laki', '08123112313', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pendaftarans`
--

CREATE TABLE `pendaftarans` (
  `id` int(10) UNSIGNED NOT NULL,
  `identitas_diri` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `waktu_pendaftaran` datetime NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT 'MENUNGGU',
  `diagnosa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `waktu_mulai_periksa` datetime DEFAULT NULL,
  `waktu_selesai_periksa` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pendaftarans`
--

INSERT INTO `pendaftarans` (`id`, `identitas_diri`, `waktu_pendaftaran`, `status`, `diagnosa`, `catatan`, `waktu_mulai_periksa`, `waktu_selesai_periksa`, `created_at`, `updated_at`) VALUES
(1, '3211181409940006', '2018-05-29 20:11:27', 'SELESAI', 'ini diagnosa dokter', 'ini catatan dokter', '2018-05-29 23:16:15', '2018-05-29 23:17:17', NULL, NULL),
(2, '10212130', '2018-05-29 23:16:03', 'SELESAI', 'diare', 'jangan makan pedas', '2018-05-29 23:16:45', '2018-05-29 23:19:52', NULL, NULL),
(3, '123', '2018-05-29 23:17:56', 'BATAL', NULL, NULL, NULL, NULL, NULL, NULL),
(4, '3211181409940006', '2018-05-29 23:55:22', 'MENUNGGU', NULL, NULL, NULL, NULL, NULL, NULL),
(5, '10212130', '2018-05-29 23:56:44', 'MENUNGGU', NULL, NULL, NULL, NULL, NULL, NULL),
(6, '123', '2018-05-30 00:10:23', 'MENUNGGU', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(1) NOT NULL,
  `jam_buka` varchar(10) DEFAULT NULL,
  `waktu_tunggu` int(11) NOT NULL DEFAULT '10',
  `batas_pasien` int(3) NOT NULL DEFAULT '50',
  `status_antrian` varchar(10) DEFAULT NULL,
  `status_pointer` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `jam_buka`, `waktu_tunggu`, `batas_pasien`, `status_antrian`, `status_pointer`) VALUES
(1, '09:00', 10, 5, 'open', 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', 'admin123', NULL, NULL, NULL),
(2, 'indra', 'indrahadisetiadi94@gmail.com', '$2y$10$Uh9TEgf58yVqZMDfpcz2GujbloIF65zUc02mrIpTzTDMRfvjZ/dTW', 'xFm0rtYKWLkU7P88BvAFHd3CBICwjfUBFdQtlvEGRmml1xOdda00M0aY2hm7', '2018-05-02 23:55:06', '2018-05-02 23:55:06'),
(3, 'admin', 'admin@admin.com', '$2y$10$AvHrnkO7iqLhlgiWcZhh2OLWsFYvv7Sdd8l1QFiDh8tpe7Utt3eqa', 'RJaaSyJsdPuaWq8c4ozwMussUrXOPWefyBXyIXWfiQ4akfWUePkbuGMNC7Q1', '2018-05-26 00:54:31', '2018-05-26 00:54:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pasiens`
--
ALTER TABLE `pasiens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pasiens_identias_diri_unique` (`identitas_diri`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pendaftarans`
--
ALTER TABLE `pendaftarans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pendaftarans_id_unique` (`id`),
  ADD KEY `pendaftarans_identitas_diri_index` (`identitas_diri`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `pasiens`
--
ALTER TABLE `pasiens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pendaftarans`
--
ALTER TABLE `pendaftarans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
